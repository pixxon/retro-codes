cmake_minimum_required(VERSION 3.12)

project(retro-codes
    LANGUAGES CXX
)

add_subdirectory(game-of-life)
add_subdirectory(generator)
add_subdirectory(kruskal)
add_subdirectory(maze)
add_subdirectory(sierpinski)
add_subdirectory(snake)
