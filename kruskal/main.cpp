#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <vector>
#include <SDL2/SDL.h>


typedef enum { Way, Wall, Exit }              Cell;
typedef std::vector<std::vector<Cell> > Palya;



std::vector<SDL_Rect>               walls;
std::vector<std::vector<SDL_Rect> > sets;
Palya                               maze;
SDL_Event                           event;
SDL_Window*                         window;
SDL_Surface*                        screen;


void CreateMaze (  );
void DrawMaze   (  );
void Generate   (  );
void mixVec     (  );

int  getID      ( SDL_Rect               a );
void unite      ( std::vector<SDL_Rect> &a, std::vector<SDL_Rect>               &b );
bool elem       ( SDL_Rect               a, std::vector<SDL_Rect>               &b );


int main( int argc, char** argv ) {

    SDL_Init( SDL_INIT_VIDEO );
    srand( time( 0 ) );


    CreateMaze (  );
    mixVec     (  );


    window = SDL_CreateWindow     ( "Generate", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, maze[ 0 ].size() * 10, maze.size() * 10, SDL_WINDOW_SHOWN);
    screen = SDL_GetWindowSurface ( window );


    DrawMaze (  );
    Generate (  );

    maze[ 1 ][ 0 ] = Way;
    maze[ maze.size() - 2 ][ maze[ 0 ].size() - 1 ] = Way;

    DrawMaze (  );

    while( event.type != SDL_QUIT && ( event.type != SDL_KEYDOWN && event.key.keysym.sym != SDLK_ESCAPE ) )
    {
        SDL_PollEvent( &event );
    };

    SDL_FreeSurface   ( screen );
    SDL_DestroyWindow ( window );
    return 0;
}


void CreateMaze(  )
{
    maze.resize( 25 );
    for ( int i = 0; i < (int)maze.size() ; i++ )
    {
        maze[ i ].resize( 35 );
        for ( int j = 0; j < (int)maze[ i ].size() ; j++ )
        {
            maze[ i ][ j ] = Wall;
        }
    }

    for ( int i = 0; i < (int)maze.size() / 2; i++ )
    {
        for ( int j = 0; j < (int)maze[ i ].size() / 2; j++ )
        {
            SDL_Rect temp;
            temp.x = 2 * i;
            temp.y = 2 * j + 1;
            if ( temp.y != (int)maze[ i ].size() - 1 && temp.x != 0 )
                walls.push_back( temp );
            temp.x = 2 * i + 1;
            temp.y = 2 * j;
            if ( temp.x != (int)maze.size() - 1 && temp.y != 0 )
                walls.push_back( temp );

            std::vector<SDL_Rect> temp2;
            temp2.clear();
            temp.x = 2 * i + 1;
            temp.y = 2 * j + 1;
            temp2.push_back( temp );
            sets.push_back( temp2 );
        }
    }
}

void DrawMaze(  )
{
    std::vector<SDL_Rect> way;
    way.resize( 0 );
    for ( int i = 0; i < (int)sets.size(); i++ )
    {
        if ( sets[i].size() > 1 )
            unite ( way,sets[i] );
    }

    for ( int k = 0; k < (int)way.size(); k++ )
    {
        maze[ way[ k ].x ][ way[ k ].y ] = Way;
    }

    for ( int i = 0; i < (int)maze.size(); ++i )
    {
        for ( int j = 0; j < (int)maze[ i ].size(); ++j )
        {
            SDL_Rect pos;
            pos.x = 10 * j;
            pos.y = 10 * i;

            SDL_Surface* tex;
            switch( maze[ i ][ j ] )
            {
                case Wall:
                    tex = SDL_LoadBMP( "images/wall.bmp" );
                break;
                case Way:
                    tex = SDL_LoadBMP( "images/bg.bmp" );
                break;
                case Exit:
                    tex = SDL_LoadBMP( "images/goal.bmp" );
                break;
            }
            SDL_BlitSurface ( tex, 0, screen, &pos );
        }
    }
    SDL_UpdateWindowSurface ( window );
    SDL_PollEvent ( &event );
}

void Generate(  )
{
    int k = 0;
    while ( k < (int)walls.size() && sets.size() != 1 )
    {
        SDL_Rect temp;
        if ( walls[k].x % 2 == 1 )
        {
            temp.x = walls[ k ].x;
            temp.y = walls[ k ].y - 1;
            int temp2 = getID ( temp );
            temp.x = walls[ k ].x;
            temp.y = walls[ k ].y + 1;
            int temp3 = getID ( temp );
            if ( temp2 != temp3 )
            {
                sets[ temp2 ].push_back( walls[ k ] );
                unite ( sets[ temp2 ],sets[ temp3 ] );
                sets.erase ( sets.begin() + temp3 );
            }
        }
        else
        {
            temp.x = walls[ k ].x - 1;
            temp.y = walls[ k ].y;
            int temp2 = getID ( temp );
            temp.x = walls[ k ].x + 1;
            temp.y = walls[ k ].y;
            int temp3 = getID ( temp );
            if ( temp2 != temp3 )
            {
                sets[ temp2 ].push_back( walls[ k ] );
                unite ( sets[ temp2 ],sets[ temp3 ] );
                sets.erase ( sets.begin() + temp3 );
            }
        }
        DrawMaze(  );
        k++;
    };
}

int getID ( SDL_Rect a )
{
    bool l = false;
    int i = 0;
    while( !l && i < (int)sets.size() )
    {
        l = elem( a, sets[ i ] );
        i++;
    };
    return i-1;
}

void unite ( std::vector<SDL_Rect> &a, std::vector<SDL_Rect> &b )
{
    for ( int i = 0; i < (int)b.size(); i++ )
    {
        a.push_back ( b[ i ] );
    }
}

bool elem( SDL_Rect a, std::vector<SDL_Rect> &b )
{
    bool l = false;
    int i = 0;
    while ( !l && i < (int)b.size() )
    {
        l = ( a.x == b[ i ].x && a.y == b[ i ].y );
        i++;
    }
    return l;
}

void mixVec(  )
{
    srand ( time( NULL ) );
    for ( int k = 0; k < (int)walls.size(); k++ )
    {
        int r = rand() % (int)walls.size();
        SDL_Rect temp = walls[ k ];
        walls[ k ] = walls[ r ];
        walls[ r ] = temp;
    }
}
