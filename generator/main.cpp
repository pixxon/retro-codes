#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <vector>
#include <SDL2/SDL.h>


typedef enum { Way, Wall, Exit}         Cell;
typedef enum { up_, down_, right_, left_ }  Direction;
typedef std::vector<std::vector<Cell> > Palya;


Palya           p;
SDL_Event       event;
SDL_Window*     window;
SDL_Surface*    screen;


void CreateMaze (  );
void DrawMaze   (  );
void MakeMove   ( int x, int y );



using namespace std;

int main( int argc, char** argv ) {

    SDL_Init( SDL_INIT_VIDEO );
    srand( time( 0 ) );


    CreateMaze(  );


    window = SDL_CreateWindow     ( "Generate", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, p.size() * 10, p[0].size() * 10, SDL_WINDOW_SHOWN);
    screen = SDL_GetWindowSurface ( window );


    DrawMaze(  );
    MakeMove( 1, 1);


    p[ 1 ][ 0 ] = Exit;
    p[ p.size() - 2 ][ p[ 0 ].size() - 1 ] = Exit;
    DrawMaze(  );
    SDL_UpdateWindowSurface( window );


    while( event.type != SDL_QUIT && ( event.type != SDL_KEYDOWN && event.key.keysym.sym != SDLK_ESCAPE ) )
    {
        SDL_WaitEvent( &event );
    }


    SDL_FreeSurface   ( screen );
    SDL_DestroyWindow ( window );
    return 0;
}


void CreateMaze(  )
{
    srand( time( 0 ) );

    p.resize( 35 );
    for ( int i = 0; i < (int)p.size(); ++i )
    {
        p[ i ].resize( 25 );
        for ( int j = 0; j < (int)p[i].size(); ++j )
          p[ i ][ j ] = Wall;
    }
}

void DrawMaze(  )
{
    for ( int i = 0; i < (int)p.size(); ++i )
    {
        for ( int j = 0; j < (int)p[ i ].size(); ++j )
        {
            SDL_Rect pos;
            pos.x = 10 * j;
            pos.y = 10 * i;

            SDL_Surface* tex;
            switch( p[ i ][ j ] )
            {
                case Wall:
                    tex = SDL_LoadBMP( "images/wall.bmp" );
                break;
                case Way:
                    tex = SDL_LoadBMP( "images/bg.bmp" );
                break;
                case Exit:
                    tex = SDL_LoadBMP( "images/goal.bmp" );
                break;
            }
            SDL_BlitSurface ( tex, 0, screen, &pos );
        }
    }
    SDL_UpdateWindowSurface ( window );
    SDL_PollEvent( &event );
}

void MakeMove( int x, int y )
{
    Direction dirs[ 4 ] = { up_, down_, left_, right_ };
    p[ x ][ y ] = Way;
    DrawMaze(  );

    for ( int i = 0; i < 4; ++i )
    {
        int r = rand() % 4;
        Direction temp = dirs[ i ];
        dirs[ i ] = dirs[ r ];
        dirs[ r ] = temp;
    }

    for ( int i = 0; i < 4; ++i )
        switch ( dirs[ i ] )
        {
            case up_:
                if ( x >= 2 && p[ x - 2 ][ y ] == Wall )
                {
                    p[ x - 1 ][ y ] = Way;
                    MakeMove( x - 2, y );
                    p[ x - 1 ][ y ] = Exit;
                }
            break;
            case left_:
                if ( y >= 2 && p[ x ][ y - 2 ] == Wall )
                {
                    p[ x ][ y - 1 ] = Way;
                    MakeMove( x, y - 2 );
                    p[ x ][ y - 1 ] = Exit;
                }
            break;
            case down_:
                if ( x < (int)p.size() - 2 && p[ x + 2][ y ] == Wall)
                {
                    p[ x + 1 ][ y ] = Way;
                    MakeMove( x + 2, y );
                    p[ x + 1 ][ y ] = Exit;
                }
            break;
            case right_:
                if ( y < (int)p[ x ].size() - 2 && p[ x ][ y+2 ] == Wall)
                {
                    p[ x ][ y+1 ] = Way;
                    MakeMove( x, y + 2 );
                    p[ x ][ y+1 ] = Exit;
                }
            break;
        }

    p[ x ][ y ] = Exit;
    DrawMaze(  );
}
