#include "header.h"

void createWall  ( std::vector<std::vector<int> > &wall  )
{
    wall.resize(100);
    for(int i=0;i<100;i++)
    {
        wall[i].resize(100);
        for(int j=0;j<100;j++)
        {
            wall[i][j]=0;
            if(i==0 || i==99 || j==0 || j==99) wall[i][j]=1;
        }
    }
}

void createSnake ( std::vector<SDL_Rect>          &snake )
{
    snake.resize(4);
    for(int i=0;i<4;i++)
    {
        snake[i].x=46+i;
        snake[i].y=50;
    }
}

void createFruit ( SDL_Rect                       &fruit )
{
    fruit.x=(rand() % 98 + 1);
    fruit.y=(rand() % 98 + 1);
}

void drawWall    ( SDL_Surface* screen, std::vector<std::vector<int> > &wall , SDL_Surface* wall_text )
{
    for(int i=0;i<100;i++)
    {
        for(int j=0;j<100;j++)
        {
            if(wall[i][j]==1)
            {
                SDL_Rect pos;
                pos.x=5*i;
                pos.y=5*j;

                SDL_BlitSurface(wall_text, 0, screen, &pos);
            }
        }
    }
}

void drawSnake   ( SDL_Surface* screen, std::vector<SDL_Rect>          &snake, SDL_Surface* body_text )
{
    for(int i=0;i<(int)snake.size();i++)
    {
        SDL_Rect pos;
        pos.x=5*snake[i].x;
        pos.y=5*snake[i].y;

        SDL_BlitSurface(body_text, 0, screen, &pos);
    }
}

void drawFruit   ( SDL_Surface* screen, SDL_Rect                       &fruit, SDL_Surface* fruit_text)
{
    SDL_Rect pos;
    pos.x=fruit.x*5;
    pos.y=fruit.y*5;

    SDL_BlitSurface(fruit_text,0,screen,&pos);
}

void drawScore   ( SDL_Surface* screen, std::vector<int>          &score )
{
    SDL_Rect pos;
    pos.x = 325;
    pos.y = 300;

    std::stringstream ss;

    for(int i=0;i<(int)score.size();i++)
    {
        ss.str("");
        ss.clear();
        ss << score[i];
        std::string str="images/numbers/" + ss.str() + ".bmp";
        SDL_Surface* num = SDL_LoadBMP( str.c_str() );

        SDL_BlitSurface ( num, 0, screen, &pos );

        pos.x=pos.x-50;
    }
}

void moveSnake   ( std::vector<SDL_Rect> &snake, SDL_Rect direction)
{
    for(int i=1;i<(int)snake.size();i++)
    {
        snake[i-1]=snake[i];
    }
    snake[snake.size()-1].x=snake[snake.size()-1].x+direction.x;
    snake[snake.size()-1].y=snake[snake.size()-1].y+direction.y;
}

bool elem        ( std::vector<SDL_Rect> &snake, SDL_Rect fruit    )
{
    bool l=false;
    int i=0;
    while(!l && i<(int)snake.size())
    {
        l=(fruit.x==snake[i].x && fruit.y==snake[i].y);
        i++;
    };
    return l;
}

void inc         ( std::vector<int> &score )
{
    score[0]++;
    for(int i = 0; i < 4; i++ )
    {
        if ( score[i] == 10 ) { score[i]=0; score[i+1]++; }
    }
}
