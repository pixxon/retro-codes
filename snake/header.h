#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#include <cstdlib>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <vector>
#include <string>
#include <sstream>
#include <time.h>

void createWall  ( std::vector<std::vector<int> > &wall  );
void createSnake ( std::vector<SDL_Rect>          &snake );
void createFruit ( SDL_Rect                       &fruit );

void drawWall    ( SDL_Surface* screen, std::vector<std::vector<int> > &wall , SDL_Surface* wall_text );
void drawSnake   ( SDL_Surface* screen, std::vector<SDL_Rect>          &snake, SDL_Surface* body_text );
void drawFruit   ( SDL_Surface* screen, SDL_Rect                       &fruit, SDL_Surface* fruit_text);
void drawScore   ( SDL_Surface* screen, std::vector<int>          &score );

void moveSnake   ( std::vector<SDL_Rect> &snake, SDL_Rect direction);
bool elem        ( std::vector<SDL_Rect> &snake, SDL_Rect fruit    );
void inc         ( std::vector<int>      &score);

#endif // HEADER_H_INCLUDED
