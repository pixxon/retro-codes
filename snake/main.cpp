#include "header.h"

int main ( int argc, char** argv )
{
    std::vector<std::vector<int> > wall;
    std::vector<SDL_Rect>          snake;
    std::vector<int>               score;
    SDL_Rect                       direction;
    SDL_Rect                       fruit;
    SDL_Rect                       pos;
    SDL_Event                      event;
    SDL_Window*                    window;
    SDL_Surface*                   screen;
    SDL_Surface*                   wall_text;
    SDL_Surface*                   body_text;
    SDL_Surface*                   fruit_text;
    SDL_Surface*                   bg_text;
    SDL_Surface*                   endscreen;
    Uint32                         black;


    srand( time( NULL ) );
    SDL_Init( SDL_INIT_VIDEO );

    window = SDL_CreateWindow    ( "Snake", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 500, 500, SDL_WINDOW_SHOWN);
    screen = SDL_GetWindowSurface( window );
    black  = SDL_MapRGB          ( screen->format, 0x00, 0x00, 0x00);

    endscreen  = SDL_LoadBMP( "images/endscreen.bmp" );
    wall_text  = SDL_LoadBMP( "images/wall.bmp"      );
    body_text  = SDL_LoadBMP( "images/body.bmp"      );
    fruit_text = SDL_LoadBMP( "images/fruit.bmp"     );
    bg_text    = SDL_LoadBMP( "images/bg.bmp"        );

    createWall  ( wall  );
    createSnake ( snake );
    createFruit ( fruit );

    direction.x = 1;
    direction.y = 0;

    SDL_FillRect ( screen, 0    , black      );
    drawWall     ( screen, wall , wall_text  );
    drawSnake    ( screen, snake, body_text  );
    drawFruit    ( screen, fruit, fruit_text );

    score.resize(5);
    for(int i=0;i<5;i++)
    {
        score[i]=0;
    }


    bool done      = false;
    bool game_over = false;
    while ( !done && !game_over )
    {
        SDL_PollEvent( &event );

        switch ( event.type )
        {
            case SDL_QUIT:
                done = true;
            break;
            case SDL_KEYDOWN:
                if ( event.key.keysym.sym == SDLK_ESCAPE) done = true;

                if ( event.key.keysym.sym == SDLK_UP    && direction.y !=  1) { direction.x =  0; direction.y = -1; }
                if ( event.key.keysym.sym == SDLK_DOWN  && direction.y != -1) { direction.x =  0; direction.y =  1; }
                if ( event.key.keysym.sym == SDLK_LEFT  && direction.x !=  1) { direction.x = -1; direction.y =  0; }
                if ( event.key.keysym.sym == SDLK_RIGHT && direction.x != -1) { direction.x =  1; direction.y =  0; }
            break;
        }

        pos.x = snake[ snake.size() - 1 ].x + direction.x;
        pos.y = snake[ snake.size() - 1 ].y + direction.y;

        game_over = ( 1 == wall[ pos.x ][ pos.y ] || elem( snake, pos ) );
        if(game_over)
        {
            break;
        }

        if( pos.x == fruit.x && pos.y == fruit.y )
        {
            createFruit ( fruit );

            while ( elem( snake, fruit ) )
            {
                createFruit ( fruit );
            };

            drawFruit ( screen, fruit, fruit_text );
            snake.push_back( pos );
            inc( score );
        }
        else
        {
            pos.x = snake[ 0 ].x * 5;
            pos.y = snake[ 0 ].y * 5;
            SDL_BlitSurface ( bg_text, 0, screen, &pos );
            moveSnake ( snake, direction );
        }

        pos.x = snake [ snake.size() - 1 ].x * 5;
        pos.y = snake [ snake.size() - 1 ].y * 5;
        SDL_BlitSurface ( body_text, 0, screen, &pos );

        SDL_UpdateWindowSurface( window );
        SDL_Delay( 50 );
    }

    SDL_BlitSurface ( endscreen, 0, screen, 0 );
    drawScore       ( screen,    score        );

    SDL_UpdateWindowSurface( window );

    SDL_Delay( 100 );
    while( event.key.keysym.sym!=SDLK_RETURN )
    {
        SDL_WaitEvent( &event );
    };



    SDL_FreeSurface   ( screen );
    SDL_DestroyWindow ( window );

    return 0;
}
