#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <vector>
#include <SDL2/SDL.h>

typedef enum { Way, Wall, Exit}         Cell;
typedef enum { up, down, right, left }  Direction;
typedef std::vector<std::vector<Cell> > Palya;

void CreateMaze ( Palya &p                      );
void DrawMaze   ( SDL_Surface* screen, Palya &p );
void MakeMove   ( Palya &p, int x, int y        );

#endif // HEADER_H_INCLUDED
