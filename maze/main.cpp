#include "header.h"

using namespace std;

int main( int argc, char** argv ) {
    Palya           p;
    SDL_Event       event;
    SDL_Window*     window;
    SDL_Surface*    screen;
    SDL_Surface*    tex;
    SDL_Surface*    bg;
    SDL_Rect        pos;
    SDL_Rect        cur;
    Uint32          black;
    bool            done=false;
    bool            game_over=false;
    bool            victory=false;

    SDL_Init( SDL_INIT_VIDEO );


    CreateMaze      ( p );


    window = SDL_CreateWindow     ( "aMaze", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (int)p[0].size() * 10, (int)p.size() * 10, SDL_WINDOW_SHOWN);
    screen = SDL_GetWindowSurface ( window );
    black  = SDL_MapRGB           ( screen->format, 0x00, 0x00, 0x00);
    tex    = SDL_LoadBMP          ( "images/body.bmp" );
    bg     = SDL_LoadBMP          ( "images/bg.bmp" );

    cur.x  = 0;
    cur.y  = 1;
    pos.x  = cur.x * 10;
    pos.y  = cur.y * 10;


    SDL_FillRect    ( screen, 0, black     );
    DrawMaze        ( screen, p            );
    SDL_BlitSurface ( tex, 0, screen, &pos );

    SDL_UpdateWindowSurface( window );


    while(!done && !game_over && !victory)
    {
        SDL_WaitEvent( &event );

        switch ( event.type )
        {
            case SDL_QUIT:
                done = true;
            break;
            case SDL_KEYDOWN:
                if ( event.key.keysym.sym == SDLK_ESCAPE) done = true;

                if ( event.key.keysym.sym == SDLK_UP    ) { cur.y--; }
                if ( event.key.keysym.sym == SDLK_DOWN  ) { cur.y++; }
                if ( event.key.keysym.sym == SDLK_LEFT  ) { cur.x--; }
                if ( event.key.keysym.sym == SDLK_RIGHT ) { cur.x++; }
            break;
        }


        game_over = ( Wall == p[cur.y][cur.x] );
        victory   = ( Exit == p[cur.y][cur.x] );


        SDL_BlitSurface ( bg, 0, screen, &pos );
        pos.x=cur.x * 10;
        pos.y=cur.y * 10;
        SDL_BlitSurface ( tex, 0, screen, &pos );


        if( game_over ) break;
        SDL_UpdateWindowSurface( window );
    }

    if( game_over ) cout << "GAME OVER\n\n";
    if( victory )   cout << "VICTORY\n\n";

    SDL_FreeSurface   ( screen );
    SDL_DestroyWindow ( window );

    system( "pause" );
    return 0;
}
