#include "header.h"

void CreateMaze( Palya &p )
{
    srand( time( 0 ) );

    p.resize( 15 );
    for ( int i = 0; i < (int)p.size(); ++i )
    {
        p[ i ].resize( 15 );
        for ( int j = 0; j < (int)p[i].size(); ++j )
          p[ i ][ j ] = Wall;
    }

    MakeMove( p, 1, 1 );

    p[ 1 ][ 0 ] = Way;
    p[ p.size() - 2 ][ p[ 0 ].size() - 1 ] = Exit;
}

void DrawMaze( SDL_Surface* screen, Palya &p )
{
    for ( int i = 0; i < (int)p.size(); ++i )
    {
        for ( int j = 0; j < (int)p[ i ].size(); ++j )
        {
            SDL_Rect pos;
            pos.x = 10 * j;
            pos.y = 10 * i;

            SDL_Surface* tex;
            switch( p[ i ][ j ] )
            {
                case Wall:
                    tex = SDL_LoadBMP( "images/wall.bmp" );
                break;
                case Way:
                    tex = SDL_LoadBMP( "images/bg.bmp" );
                break;
                case Exit:
                    tex = SDL_LoadBMP( "images/goal.bmp" );
                break;
            }
            SDL_BlitSurface ( tex, 0, screen, &pos );
        }
    }
}

void MakeMove( Palya &p, int x, int y )
{
    Direction dirs[ 4 ] = { up, down, left, right };
    p[ x ][ y ] = Way;

    for ( int i = 0; i < 4; ++i )
    {
        int r = rand() % 4;
        Direction temp = dirs[ i ];
        dirs[ i ] = dirs[ r ];
        dirs[ r ] = temp;
    }

    for ( int i = 0; i < 4; ++i )
        switch ( dirs[ i ] )
        {
            case up:
                if ( x >= 2 && p[ x - 2 ][ y ] == Wall )
                {
                    p[ x - 1 ][ y ] = Way;
                    MakeMove( p, x - 2, y );
                }
            break;
            case left:
                if ( y >= 2 && p[ x ][ y - 2 ] == Wall )
                {
                    p[ x ][ y - 1 ] = Way;
                    MakeMove( p, x, y - 2 );
                }
            break;
            case down:
                if ( x < (int)p.size() - 2 && p[ x + 2][ y ] == Wall)
                {
                    p[ x + 1 ][ y ] = Way;
                    MakeMove( p, x + 2, y );
                }
            break;
            case right:
                if ( y < (int)p[ x ].size() - 2 && p[ x ][ y+2 ] == Wall)
                {
                    p[ x ][ y+1 ] = Way;
                    MakeMove( p, x, y + 2 );
                }
            break;
        }
}
