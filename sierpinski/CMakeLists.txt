find_package(SDL2 REQUIRED)

set(SOURCES
    header.cpp
    main.cpp
)

set(HEADERS
    header.h
)

add_executable(sierpinksi
    ${SOURCES}
    ${HEADERS} 
)

target_include_directories(sierpinksi
    PRIVATE ${SDL2_INCLUDE_DIRS}
)

target_link_libraries(sierpinksi
   PRIVATE ${SDL2_LIBRARIES} 
)
