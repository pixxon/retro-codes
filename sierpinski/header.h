#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#include <SDL2/SDL.h>
#include <vector>
#include <iostream>

void   putpixel       ( SDL_Surface *surface, int x,    int y, Uint32 pixel );
Uint32 getpixel       ( SDL_Surface *surface, int x,    int y               );
int    szomsz         ( SDL_Surface* surface, int x,    int y, Uint32 a     );
void   ReverseSurface ( SDL_Surface* surface, Uint32 a, Uint32 b            );
void   Update         ( SDL_Surface* surface, SDL_Surface* surface2, Uint32 a, Uint32 b);

#endif // HEADER_H_INCLUDED
