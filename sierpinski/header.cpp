#include "header.h"

void putpixel( SDL_Surface *surface, int x, int y, Uint32 pixel )
{
    int bpp  = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp)
    {
        case 1:
            *p = pixel;
        break;
        case 2:
            *(Uint16 *)p = pixel;
        break;
        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            {
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }
            else
            {
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
        break;
        case 4:
            *(Uint32 *)p = pixel;
        break;
    }
}

Uint32 getpixel ( SDL_Surface *surface, int x, int y )
{
    int bpp  = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp)
    {
        case 1:
            return *p;
        break;
        case 2:
            return *(Uint16 *)p;
        break;
        case 3:
            if( SDL_BYTEORDER == SDL_BIG_ENDIAN )
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;
        break;
        case 4:
            return *(Uint32 *)p;
        break;

        default:
            return 0;
    }
}

int szomsz ( SDL_Surface* surface, int x, int y, Uint32 a )
{
    int db = 0;
    for ( int i = x - 1; i < x + 2; i++ )
    {
        for ( int j = y - 1; j < y + 2; j++ )
        {
            if ( i >= 0 && i < 500 && j >= 0 && j < 500 && ( i != x || j != y ) )
            {
                if ( getpixel ( surface, i, j ) == a )
                    db++;
            }
        }
    }
    return db;
}

void Update ( SDL_Surface* surface, SDL_Surface* surface2, Uint32 a, Uint32 b )
{
    for ( int i = 0; i < 500; i++ )
    {
        for ( int j = 0; j < 500; j++ )
        {
            Uint32 seged = getpixel ( surface, i, j );
            putpixel(  surface2, i, j, seged );

            int seg = szomsz ( surface, i, j, b );
            if ( seg == 1 && seged == a)
                putpixel ( surface2, i, j, b );

            if ( ! ( seg == 1 || seg == 2 ) && seged == b )
                putpixel ( surface2, i, j, a );
        }
    }
}

void ReverseSurface ( SDL_Surface* surface, Uint32 a, Uint32 b )
{
    for ( int i = 0; i < 500; i++ )
    {
        for ( int j = 0; j < 500; j++ )
        {
            Uint32 seged = getpixel ( surface, i, j );

            if ( seged == a )
                putpixel ( surface, i, j, b );
            else
                putpixel ( surface, i, j, a );
        }
    }
}
