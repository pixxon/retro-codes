#include "header.h"

using namespace std;

int main( int argc, char* args[] )
{
    SDL_Surface* surface;
    SDL_Surface* surface2;
    SDL_Window* window;
    SDL_Surface* screen;
    SDL_Event    event;
    int          width;
    int          height;
    Uint32       white;
    Uint32       black;
    Uint32       a;
    Uint32       b;
    Uint32       seged;
    bool         quit;


    SDL_Init ( SDL_INIT_EVERYTHING );


    surface  = SDL_LoadBMP ( "be.bmp" );
    width    = surface->w;
    height   = surface->h;
    window   = SDL_CreateWindow("sierpinski", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_FULLSCREEN | SDL_WINDOW_OPENGL);
    screen   = SDL_GetWindowSurface ( window );
    surface2 = SDL_CreateRGBSurface ( 0, width, height, 32, 0, 0, 0, 0 );
    white    = SDL_MapRGB ( screen->format, 0xff, 0xff, 0xff );
    black    = SDL_MapRGB ( screen->format, 0x00, 0x00, 0x00 );
    quit     = false;
    a        = white;
    b        = black;


    while( !quit )
    {
        SDL_PollEvent ( &event );


        Update ( surface, surface2, a, b );
        SDL_BlitSurface ( surface, NULL, screen, NULL );

        Update ( surface2, surface, a, b );
        SDL_BlitSurface ( surface, NULL, screen, NULL );


        if( event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
        {
            quit = true;
        }


        if(event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_1)
        {
            ReverseSurface ( surface, a, b );
            seged = a;
            a     = b;
            b     = seged;
            SDL_WaitEvent ( &event );
        }
    };


    SDL_FreeSurface ( surface );
    SDL_Quit (  );
    return 0;
}
