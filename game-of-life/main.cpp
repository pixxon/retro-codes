#include "header.h"

using namespace std;

int main( int argc, char* args[] )
{
    cout << "Press ESC to exit.\nPress 1 for more fun!\n\nYou have to click on the window before pressing any buttons.\n\n\n";

    string      filename;
    vector<int> birth;
    vector<int> survive;
    string      str;

    cout << "Input bmp (be.bmp) : ";
    cin >> filename;

    cout << "Birth rule (1) : ";
    cin >> str;
    Generate ( str, birth );

    cout << "Survive rule (12) : ";
    cin >> str;
    Generate ( str, survive );

    SDL_Window*  window;
    SDL_Surface* screen;
    SDL_Surface* surface;
    SDL_Surface* surface2;
    SDL_Event    event;
    Uint32       a;
    Uint32       b;
    Uint32       seged;
    bool         quit;

    SDL_Init ( SDL_INIT_VIDEO );

    surface  = SDL_LoadBMP ( filename.c_str() );
    surface2 = SDL_CreateRGBSurface ( 0, surface->w, surface->h, 32, 0, 0, 0, 0 );
    window   = SDL_CreateWindow ( "MAGIC", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, surface->w, surface->h, SDL_WINDOW_INPUT_FOCUS );
    screen   = SDL_GetWindowSurface ( window );
    a        = SDL_MapRGB ( surface->format, 0xff, 0xff, 0xff );
    b        = SDL_MapRGB ( surface->format, 0x00, 0x00, 0x00 );
    quit     = false;

    while( !quit )
    {
        SDL_PollEvent ( &event );

        Update ( surface, surface2, a, b, birth, survive );
        SDL_BlitSurface ( surface, NULL, screen, NULL );
        SDL_UpdateWindowSurface ( window );

        Update ( surface2, surface, a, b, birth, survive );
        SDL_BlitSurface ( surface, NULL, screen, NULL );
        SDL_UpdateWindowSurface ( window );


        if( event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
        {
            quit = true;
        }


        if(event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_1)
        {
            ReverseSurface ( surface, a, b );
            seged = a;
            a     = b;
            b     = seged;
        }
    };

    SDL_FreeSurface ( surface );
    SDL_DestroyWindow ( window );
    return 0;
}
