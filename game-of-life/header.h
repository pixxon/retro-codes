#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#include <SDL2/SDL.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stdlib.h>

void putpixel       ( SDL_Surface* surface, int &x,    int &y, Uint32 &pixel );
int szomsz          ( SDL_Surface* surface, int &x,    int &y, Uint32 &a     );
Uint32 getpixel     ( SDL_Surface* surface, int &x,    int &y );
void ReverseSurface ( SDL_Surface* surface, Uint32 &a, Uint32 &b );
void Update         ( SDL_Surface* surface, SDL_Surface* surface2, Uint32 &a, Uint32 &b, std::vector<int> &birth, std::vector<int> &survive );
void Generate       ( std::string &str, std::vector<int> &a );
bool elem           ( int &n,           std::vector<int> &a );

#endif // HEADER_H_INCLUDED
